package com.emids.Entity;

/**
 * Entity for Person.
 * 
 * @author SUHAS
 */
public class Person {

	/**
	 * stores name of the person,
	 */
	private String name;
	
	/**
	 * stores gender of the person,
	 */
	private char gender;
	
	/**
	 * stores age of the person,
	 */
	private int age;
	
	/**
	 * stores habits of the person,
	 */
	private Habits habits;
	
	/**
	 * stores health of the person,
	 */
	private Health health;

	/**
	 * Retrieves name of the person.
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets name of the person.
	 * @param name - name of the person
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * Retrieves gender of the person.
	 * @return gender
	 */

	public char getGender() {
		return gender;
	}
	
	/**
	 * Sets gender of the person.
	 * @param gender - gender of the person
	 */
	
	public void setGender(char gender) {
		this.gender = gender;
	}
	
	/**
	 * Retrieves age of the person.
	 * @return age
	 */
	public int getAge() {
		return age;
	}
	
	/**
	 * Sets age of the person.
	 * @param age - age of the person.
	 */
	public void setAge(int age) {
		this.age = age;
	}
	
	/**
	 * Retrieves habits of the person.
	 * @return habits
	 */
	public Habits getHabits() {
		return habits;
	}
	
	/**
	 * Sets habits of the person
	 * @param habits - habits of the person
	 */
	public void setHabits(Habits habits) {
		this.habits = habits;
	}
	
	/**
	 * Retrieves health of the person.
	 * @return health
	 */
	public Health getHealth() {
		return health;
	}
	
	/**
	 * Sets health of the person
	 * @param health - health of the person.
	 */
	public void setHealth(Health health) {
		this.health = health;
	}

}
