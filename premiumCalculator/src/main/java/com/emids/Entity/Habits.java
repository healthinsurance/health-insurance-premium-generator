package com.emids.Entity;

public class Habits {
	/**
	 * smoking habit of the person.
	 */
	private Boolean smoking;
	/**
	 * alcohol habit of the person.
	 */
	private Boolean alchohol;
	/**
	 * dailyExercise habit of the person.
	 */
	private Boolean dailyExercise;
	/**
	 * drug habit of the person.
	 */
	private Boolean drugs;
	
	/**
	 * Retrieves smoking habit of the person.
	 * @return smoking
	 */
	public Boolean getSmoking() {
		return smoking;
	}
	
	/**
	 * Sets smoking habit of the person.
	 * @param smoking
	 */
	public void setSmoking(Boolean smoking) {
		this.smoking = smoking;
	}
	
	/**
	 * Retrieves alcohol habit of the person.
	 * @return alcohol
	 */
	public Boolean getAlchohol() {
		return alchohol;
	}
	
	/**
	 * Sets alcohol habit of the person.
	 * @param alchohol
	 */
	public void setAlchohol(Boolean alchohol) {
		this.alchohol = alchohol;
	}
	
	/**
	 * Retrieves dailyExercise habit of the person.
	 * @return dailyExercise
	 */
	public Boolean getDailyExercise() {
		return dailyExercise;
	}
	
	/**
	 * Sets dailyExercise habit of the person.
	 * @param dailyExercise
	 */
	public void setDailyExercise(Boolean dailyExercise) {
		this.dailyExercise = dailyExercise;
	}
	
	/**
	 * Retrieves drugs habit of the person.
	 * @return drugs
	 */
	public Boolean getDrugs() {
		return drugs;
	}
	
	/**
	 * Sets drugs habit of the person.
	 * @param drugs
	 */
	public void setDrugs(Boolean drugs) {
		this.drugs = drugs;
	}

	

}
