package com.emids.EntityImplementation;

import com.emids.Entity.Person;

/**
 * Functional Interface for calculating Premium percentage.
 * @author SUHAS
 */
@FunctionalInterface
public interface PremiumCalculator {
	
	/**
	 * Method to calculate Premium as per condition.
	 * @param person - Input person object
	 * @param condition - Conditon to calculate premium amount
	 * @param premiumAmount - premium amount calculated
	 */
	public double calculate(Person person, String condition, double premiumAmount);
	
}
