package com.emids.EntityImplementation;

public class premiumConstants {
	public static int BASE_PREMIUM = 5000;
	public static int AGE_18_TO_25 = 10;
	public static int AGE_25_TO_30 = 10;
	public static int AGE_30_TO_35 = 10;
	public static int AGE_35_TO_40 = 10;
	public static int AGE_40_PLUS = 20;
	
	public static int AGE_18 = 18;
	public static double INCREASE_PREMIUM_EVERY_FIVE_YEARS = 5;
	
	public static int BODY_CONDITION_PREMIUM = 1;
	public static int MALE_PREMIUM = 2;
	public static int HABITS_PREMIUM = 3;
	
	public static char GENDER_MALE = 'M';
	public static char GENDER_FEMALE = 'F';
	
	
	public static String CALCULATE_AGE = "AGE";
	public static String CALCULATE_GENDER = "GENDER";
	public static String CALCULATE_BODY_CONDITION = "BODY CONDITION";
	public static String CALCULATE_HABITS = "HABITS";
}