package com.emids.EntityImplementation;

import com.emids.Entity.Habits;
import com.emids.Entity.Health;
import com.emids.Entity.Person;

public class InsuranceImpl {

	public static void main(String[] args) {
		
		InsuranceImpl insuranceImpl = new InsuranceImpl();
		Person person = new Person();
		
		insuranceImpl.assignDefaultValuesForPerson(person);

		double premium = new InsurancePremiumCalculator().calculatePremiumPercentage(person);

		System.out.println("Health Insurance Premium for "+ person.getName() + ": " + (int) premium );
	}
	
	private void assignDefaultValuesForPerson(Person person) {
		person.setName("Mr. Gomes");
		person.setGender(premiumConstants.GENDER_MALE);
		person.setAge(34);

		Health healthDetails = new Health();
		healthDetails.setHiperTension(Boolean.FALSE);
		healthDetails.setBloodPressure(Boolean.FALSE);
		healthDetails.setBloodSugar(Boolean.FALSE);
		healthDetails.setOverWeight(Boolean.TRUE);

		person.setHealth(healthDetails);

		Habits habitDetails = new Habits();
		habitDetails.setAlchohol(Boolean.TRUE);
		habitDetails.setDailyExercise(Boolean.TRUE);
		habitDetails.setDrugs(Boolean.FALSE);
		habitDetails.setSmoking(Boolean.FALSE);

		person.setHabits(habitDetails);

	}

}
