package com.emids.test.premiumCalculator;

import com.emids.Entity.Habits;
import com.emids.Entity.Health;
import com.emids.Entity.Person;
import com.emids.EntityImplementation.InsurancePremiumCalculator;
import com.emids.EntityImplementation.premiumConstants;

import junit.framework.TestCase;

public class InsurancePremiumCalculatorTest extends TestCase {

	/**
	 * Test Case for Person.
	 */
	public void testCalculate() {

		Person person = new Person();
		assignDefaultValuesForPerson(person);

		double premium = new InsurancePremiumCalculator().calculatePremiumPercentage(person);
		assertTrue(premium == 6856);
	}

	/**
	 * 
	 * @param person
	 */
	private void assignDefaultValuesForPerson(Person person) {
		person.setName("Mr. Gomes");
		person.setGender(premiumConstants.GENDER_MALE);
		person.setAge(34);

		Health healthDetails = new Health();
		healthDetails.setHiperTension(Boolean.FALSE);
		healthDetails.setBloodPressure(Boolean.FALSE);
		healthDetails.setBloodSugar(Boolean.FALSE);
		healthDetails.setOverWeight(Boolean.TRUE);

		person.setHealth(healthDetails);

		Habits habitDetails = new Habits();
		habitDetails.setAlchohol(Boolean.TRUE);
		habitDetails.setDailyExercise(Boolean.TRUE);
		habitDetails.setDrugs(Boolean.FALSE);
		habitDetails.setSmoking(Boolean.FALSE);

		person.setHabits(habitDetails);

	}

}
